import { Dm2Page } from './app.po';

describe('dm2 App', function() {
  let page: Dm2Page;

  beforeEach(() => {
    page = new Dm2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
