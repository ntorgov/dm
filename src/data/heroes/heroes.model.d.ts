/**
 * Hero model interface
 * @name IHeroModel
 */
interface IHeroModel {

	/**
	 * Hero ID
	 * @type {number}
	 */
	id: number;

	/**
	 * Hero name
	 * @type {string}
	 */
	name: string;

	/**
	 * Hero avatar
	 * @type object
	 */
	avatar: {

		/**
		 * Un sleeping avatar path
		 * @type {string}
		 */
		normal: string;

		/**
		 * Sleeping avatar path
		 * @type {string}
		 */
		sleep: string;
	};

	/**
	 * Hero sound
	 * @type {string}
	 */
	sound?: string;

	characteristics?: {

		strength?: number;
		agility?: number;
		vitality?: number;
		stamina?: number;
	}
}
