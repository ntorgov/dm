import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import { IntroComponent } from './intro/intro.component';
import { CharViewShortComponent } from './char-view-short/char-view-short.component';
import { PlaygroundComponent } from './playground/playground.component';

@NgModule({
	declarations: [
		AppComponent,
		IntroComponent,
		CharViewShortComponent,
		PlaygroundComponent,
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
