import {Component, OnInit, Input, Output} from '@angular/core';
import {DataBase} from '../../data/database';

@Component({
	selector: 'app-char-view-short',
	templateUrl: './char-view-short.component.html',
	styleUrls: ['./char-view-short.component.less'],
	// styles: [`.someTemplate{background-image:url(${DataBase.Interface.Character});}`]
})
export class CharViewShortComponent implements OnInit {

	// placement: number;

	@Input() placement: number;

	@Output() positionClass: string;

	constructor() {

		// console.log("key: " + this.placement);
	}

	ngOnInit() {
		this.positionClass = "charPanelPosition" + this.placement;
		// console.log("key1: " + this.placement);
		// this.placement = 2;
	}

}
