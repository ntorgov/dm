/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CharViewShortComponent } from './char-view-short.component';

describe('CharViewShortComponent', () => {
  let component: CharViewShortComponent;
  let fixture: ComponentFixture<CharViewShortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharViewShortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharViewShortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
